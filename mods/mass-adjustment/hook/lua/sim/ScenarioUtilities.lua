
local config = import('/mods/mass-adjustment/config.lua');

-- BREAKING HOOK: BAD PRACTICE
function CreateWreckage(unit, needToRotate)
    prop = unit:CreateWreckageProp(0)

    -- adjust the mass values
    local bp = unit:GetBlueprint()
    local mass = bp.Economy.BuildCostMass * (config.initialReclaimValue or 0)
    local energy = bp.Economy.BuildCostEnergy * (config.initialReclaimValue or 0)
    local time = (bp.Wreckage.ReclaimTimeMultiplier or 1)
    prop:SetMaxReclaimValues(time, mass, energy)

    if needToRotate then 

        -- determine a random rotation
        local roll = 0.5 + Random() - 2 * Random(0, 1)
        local pitch = 0.5 + Random() - 2 * Random(0, 1)
        local yaw = 0

        -- get the current rotation
        local unitRotation = unit:GetOrientation()
        local rotation = EulerToQuaternion(roll, pitch, yaw)
        local newOrientation = {}
        
        -- rotate them 
        newOrientation[1] = unitRotation[4] * rotation[1] + unitRotation[1] * rotation[4] + unitRotation[2] * rotation[3] - unitRotation[3] * rotation[2]
        newOrientation[2] = unitRotation[4] * rotation[2] + unitRotation[2] * rotation[4] + unitRotation[3] * rotation[1] - unitRotation[1] * rotation[3]
        newOrientation[3] = unitRotation[4] * rotation[3] + unitRotation[3] * rotation[4] + unitRotation[1] * rotation[2] - unitRotation[2] * rotation[1]
        newOrientation[4] = unitRotation[4] * rotation[4] - unitRotation[1] * rotation[1] - unitRotation[2] * rotation[2] - unitRotation[3] * rotation[3]
        
        -- set the new rotation
        prop:SetOrientation(newOrientation, true)
    end
    unit:Destroy()
end