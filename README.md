# Proof of concept

A proof of concept in the game Supreme Commander that initial wreckages can have different reclaim values than wreckages throughout the game. You can change the wreckage multiplier by changing the variables in the [config.lua](mods/mass-adjustment/config.lua) file. As an example, changing it to:

``` lua 
globalReclaimValue = 0.5
initialReclaimValue = 0.9
```

Would make the initial wreckages be worth 0.9 of their value, where as wreckages made throughout the game would be worth 0.5 of their value.

# Imagery

![](images/example-mass-adjustment-mod.png)

A show case of the mod in action: 
 - The 'initial' reclaim in seton's is changed by a factor of 0.1.
 - The 'dynamic' reclaim of wreckages is changed by a factor of 2.0. See also the mantis marked in blue.

# Issues

The code doesn't keep track of whether a unit is underwater or not - which should reduce the value of the wreck by another half. This is not an issue if this is added to the actual code base.

# Disclaimer

This proof of concept uses bad practices, such as:
 - A breaking hook
